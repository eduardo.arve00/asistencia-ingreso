/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.ingresar;

import com.griaule.fingerprintsdk.sample.FormFingerVerify;
import controldeasistencias.controllers.AsistenciaJpaController;
import controldeasistencias.controllers.EmpleadoJpaController;
import controldeasistencias.entidades.Asistencia;
import controldeasistencias.entidades.Empleado;
import controldeasistencias.entidades.Horario;
import controldeasistencias.entidades.HorarioEmp;
import controldeasistencias.utils.FuncionesUtiles;
import controldeasistencias.utils.splashscreen.ScreenUtility;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import persistence.util.JpaUtil;

/**
 *
 * @author Administrador
 */
public class IngresarAsistencia extends javax.swing.JFrame {

    static long timeInit = 0;      //tiempo desde que se captura el empleado
    static long diff = 0;   //tiempo actual. se usa para calcular la diferencia.
    static long timeWait = 4000; //4 segundos para ver la inforamcion del empleado

    static int accionSeleccionada = -1;
    private static final int ENTRAR = 1;
    private static final int SALIR = 2;
    /**
     * Creates new form IngresarAsistencia
     */
    private static EmpleadoJpaController controller;
    private static AsistenciaJpaController asistenciaController;
    private static Date date;
    private static DefaultTableModel model;
    private static Empleado emp = null;
    private static Horario horario = null;
    private static HorarioEmp horarioEmpleado = null;

    FormFingerVerify finger = null;

    static List<Asistencia> list;

    public IngresarAsistencia(java.awt.Frame parent, boolean modal) {

        initComponents();
        iniciarTabla();

        //jButton1.setVisible(false);
        controller = new EmpleadoJpaController(JpaUtil.getEntityManagerFactory());
        asistenciaController = new AsistenciaJpaController(JpaUtil.getEntityManagerFactory());
        date = new Date();
        System.out.println("Dia: " + getDayOfTheWeek(date));
        finger = new FormFingerVerify(1);
        finger.setSize(288, 313);
        panelBase.add(finger, BorderLayout.CENTER);
        lblInformacion.setText("");
        pnlInformacion.setBackground(new Color (224, 223, 227));
        
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    while (true) {
                        date.setTime(System.currentTimeMillis());
                        lblHora.setText(FuncionesUtiles.getDateToStringWhitSecongs(date));
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException ie) {
                }
            }

        }).start();

        this.setPreferredSize(ScreenUtility.getDimensionScreen());
        this.setMaximumSize(ScreenUtility.getDimensionScreen());
        this.setSize(ScreenUtility.getDimensionScreen());
        this.setLocationRelativeTo(this);

        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                finger.destroy();
                System.exit(0);
            }
        });

        cargarAsistencias();
    }

    public static int getDayOfTheWeek(Date d) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(d);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    private static HorarioEmp getHorarioEmpleado() {
        HorarioEmp hora = null;
        if (!emp.getHorarioEmpCollection().isEmpty()) {
            for (HorarioEmp h : emp.getHorarioEmpCollection()) {
                horarioEmpleado = hora = h;
            }
        }
        return hora;
    }

    public static Horario getHorarioByDay(int day) {
        Horario myHorario = null;
        switch (day) {
            case 1: //DOMINGO
                myHorario = horarioEmpleado.getDomingo();
                break;
            case 2: //LUNES
                myHorario = horarioEmpleado.getLunes();
                break;
            case 3: //MARTES
                myHorario = horarioEmpleado.getMartes();
                break;
            case 4: //MIERCOLES
                myHorario = horarioEmpleado.getMiercoles();
                break;
            case 5: //JUEVES
                myHorario = horarioEmpleado.getJueves();
                break;
            case 6: //VIERNES
                myHorario = horarioEmpleado.getViernes();
                break;
            case 7: //SABADO
                myHorario = horarioEmpleado.getSabado();
                break;
        }
        return myHorario;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblCedula = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblApellido = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblTelefono = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblFNac = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblFIngreso = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblModalidad = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblModDesc = new javax.swing.JLabel();
        panelBase = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblResultados = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        pnlInformacion = new javax.swing.JPanel();
        lblInformacion = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lblHora = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("INGRESO DE ASISTENCIA");
        setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("DATOS DEL EMPLEADO"));

        jLabel2.setText("CÉDULA:");

        lblCedula.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblCedula.setText("                                                          ");

        lblNombre.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblNombre.setText("                                                          ");

        jLabel3.setText("NOMBRE:");

        lblApellido.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblApellido.setText("                                                          ");

        jLabel4.setText("APELLIDO:");

        lblTelefono.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTelefono.setText("                                                          ");

        jLabel5.setText("TEL:");

        jLabel6.setText("F. NAC.:");

        lblFNac.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblFNac.setText("                                                          ");

        jLabel7.setText("F. ING.:");

        lblFIngreso.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblFIngreso.setText("                                                          ");

        jLabel8.setText("MODALIDAD:");

        lblModalidad.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblModalidad.setText("                                                          ");

        jLabel9.setText("MOD. DESC.:");

        lblModDesc.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblModDesc.setText("                                                          ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblModDesc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblModalidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFIngreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFNac, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTelefono, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblApellido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblCedula, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblCedula))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lblNombre))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lblApellido))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lblTelefono))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFNac, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lblFIngreso))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(lblModalidad))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(lblModDesc))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelBase.setBorder(javax.swing.BorderFactory.createTitledBorder("Huella"));
        panelBase.setLayout(new java.awt.BorderLayout());

        jButton3.setText("Salir");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        tblResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblResultadosMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tblResultadosMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblResultadosMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblResultadosMouseReleased(evt);
            }
        });
        tblResultados.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblResultadosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblResultados);

        jButton1.setText("TEST");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        pnlInformacion.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblInformacion.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblInformacion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInformacion.setText("INFORMACION");

        javax.swing.GroupLayout pnlInformacionLayout = new javax.swing.GroupLayout(pnlInformacion);
        pnlInformacion.setLayout(pnlInformacionLayout);
        pnlInformacionLayout.setHorizontalGroup(
            pnlInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblInformacion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlInformacionLayout.setVerticalGroup(
            pnlInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInformacionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblInformacion, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(0, 0, 0));

        lblHora.setBackground(new java.awt.Color(0, 0, 0));
        lblHora.setFont(new java.awt.Font("Consolas", 1, 36)); // NOI18N
        lblHora.setForeground(new java.awt.Color(0, 204, 0));
        lblHora.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblHora.setText("HORA");
        lblHora.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblHora, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblHora, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(56, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 537, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(pnlInformacion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(panelBase, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(56, Short.MAX_VALUE))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelBase, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlInformacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton1))
                .addGap(37, 37, 37))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cargarAsistencias() {
        list = asistenciaController.findAsistencia(new Date());
        System.out.println("Count: " + list.size());
    }

    public static void agregarEntSalida(int id) {

        obtenerEmpleado(id);
        mostrarDatos();

        getHorarioEmpleado();
        nuevoRegistro();

        cargarTabla(list);

        quitarInfo();
    }

    static void cargarTabla(List list) {
        limpiarTabla();
        Asistencia asis;
        for (Object object : list) {
            asis = (Asistencia) object;
            model.addRow(new Object[]{asis.getNombreCompleto(), asis.getCedula(),
                (asis.getHoraIng() != null) ? FuncionesUtiles.getTimeHHmm(asis.getHoraIng()) : " ",
                (asis.getHoraSal() != null) ? FuncionesUtiles.getTimeHHmm(asis.getHoraSal()) : " "});
        }
    }

    static void limpiarTabla() {
        while (model.getRowCount() > 0) {
            model.removeRow(0);
        }
    }

    /**
     * determina la diferencia del horario. return 0 si la entrada se produce
     * dentro del rango horaInicio y horaT > 0 si la diferencia de horario es
     * mayor que la horaT.
     * <br>Retrazos para entrada
     * <br>Tiempo Extra para salida. 0 < si se produce una entrada temprana. o u
     */
    private static int diffHorario(Date horaInicio, Date horaT) {
        long timeNow = date.getTime() / 1000;
        long timeIn = setDateToday(horaInicio);
        long timeInTl = setDateToday(horaT);

        int res = 0;

        //si esta dentro del rango de la hora entrada a  la tolerancia.
        if (timeNow >= timeIn && timeNow <= timeInTl) {
            //    System.out.println("REGISTRO VALIDO");

            res = 0;

        } else if (timeNow < timeIn) {
            //si el tiempo de entrada es menor que la hora que le toca entrar
            //   System.out.println("HA ENTRADO O SALIDO ANTES");
            res = (int) (timeNow - timeIn);

        } else if (timeNow > timeInTl) {
            //    System.out.println("HA ENTRADO O SALIDOR DESPUES");
            //si el registro se produce despues de la tolerancia. 
            //ha ingresado despues de la hora que le corresponde o 
            //ha salido mas tarde de lo que debe.
            res = (int) (timeNow - timeInTl);
        }

        return res / 60;
    }

    private static long setDateToday(Date hora) {
        Calendar c1 = Calendar.getInstance();
        int diaActual = c1.get(Calendar.DAY_OF_YEAR);
        int mesActual = c1.get(Calendar.MONTH);
        int anioActual = c1.get(Calendar.YEAR);

        c1.setTime(hora);
        c1.set(Calendar.DAY_OF_YEAR, diaActual);
        c1.set(Calendar.YEAR, anioActual);
        c1.set(Calendar.MONTH, mesActual);
        //System.out.println(c1.getTime());

        return (c1.getTimeInMillis() / 1000);
    }

    static void nuevoRegistro() {
        Horario myHorario;
        //obtiene el horario que le corresponde al empleado para el dia de semana en curso
        try {
            myHorario = getHorarioByDay(getDayOfTheWeek(date));
        } catch (java.lang.NullPointerException ne) {
            lblInformacion.setText("No tiene horario asignado");
            pnlInformacion.setBackground(Color.red);
            return;
        }

        //se obtiene la diferencia en minutos
        int minutosDiferencia = 0;

        //compribar si ha ingresado 
        //actualizar el registro para colocar la hora de salida
        for (Asistencia asis : list) {

            if (asis.getCedula() == emp.getCedula()) {
                if (asis.getHoraSal() == null) {
                    //ya ha salido. omitir el nuevo registro.
                    minutosDiferencia = diffHorario(myHorario.getSalida(), myHorario.getHSTolerancia());

                    System.out.println("MINUTOS: " + minutosDiferencia);
                    System.out.print("REGISTRO DE SALIDA: ");
                    asis.setHoraSal(new Date());
                    //si el timpo de salida es mayor que al que le corresponde
                    if (minutosDiferencia > 0) {
                        System.out.println("TIEMPO EXTRA ");
                        //anexar tiempo extra
                        asis.setMinutosExtras(minutosDiferencia);
                    }
                    try {
                        asistenciaController.edit(asis);
                    } catch (Exception ex) {
                        Logger.getLogger(IngresarAsistencia.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                    cargarTabla(list);
                    return;
                } else {
                    return;
                }
            }
        }
        minutosDiferencia = diffHorario(myHorario.getEntrada(), myHorario.getHETolerancia());
        //si pasa es porque no se ha registrado. 
        //sino registrar el ingreso. 
        Asistencia nuevaAsistencia = new Asistencia();
        nuevaAsistencia.setIdEmpleado(emp.getId());
        nuevaAsistencia.setNombreCompleto(emp.getNombres() + ", " + emp.getApellido());
        nuevaAsistencia.setCedula(emp.getCedula());
        nuevaAsistencia.setFecha(new Date());
        nuevaAsistencia.setHoraIng(new Date());
        //si el timpo de entrada es mayor que al que le corresponde
        if (minutosDiferencia == 0) {
            System.out.println("ENTRA DENTRO DEL TIEMPO tiempo que le corresponde");
            //anexar minutos de retrazo
            nuevaAsistencia.setMinutosRetrazo(minutosDiferencia);
            System.out.println("MINUTOS: " + minutosDiferencia);
            nuevaAsistencia.setIdMHorario(1);
            list.add(nuevaAsistencia);
            asistenciaController.create(nuevaAsistencia);
            
            //informar al usuario que no se tomara su asistencia.
            pnlInformacion.setBackground(Color.GREEN);
            lblInformacion.setText("Bienvenido");
        } else {
            //informar al usuario que no se tomara su asistencia.
            pnlInformacion.setBackground(Color.ORANGE);
            lblInformacion.setText("Has ingresado demasiado tarde");
        }
    }

    static void quitarInfo() {
        //almacena el tiempo en el que se recibe la notificacion.
        timeInit = System.currentTimeMillis();
        new Thread(new Runnable() {

            @Override
            public void run() {
                boolean flag = true;
                while (true) {

                    diff = System.currentTimeMillis() - timeInit;
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                    }
                    //si han pasado mas del tiempo estimado la info se quita.
                    if (diff >= timeWait) {
                        limpiarCampos();
                        flag = false;
                        diff = 0;
                        timeInit = 0;
                    }

                }
            }

        }).start();
    }

    static Empleado obtenerEmpleado(int id) {
        return emp = controller.findEmpleado(id);
    }

    public void iniciarTabla() {
        String[] columnNames = {"NOMBRE COMPLETO", "CEDULA", "HORA ENTRADA", "HORA SALIDA"};

        model = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        model.setColumnIdentifiers(columnNames);

        tblResultados.setModel(model);
    }

    static void limpiarCampos() {
        lblCedula.setText("");
        lblNombre.setText("");
        lblApellido.setText("");
        lblTelefono.setText("");
        lblFNac.setText("");
        lblFIngreso.setText("");
        lblModDesc.setText("");
        lblModalidad.setText("");
        lblInformacion.setText("");
        pnlInformacion.setBackground(new Color (224, 223, 227));
    }

    static void mostrarDatos() {
        lblCedula.setText(emp.getCedula());
        lblNombre.setText(emp.getNombres());
        lblApellido.setText(emp.getApellido());
        lblTelefono.setText(emp.getTelefono());
        lblFNac.setText(FuncionesUtiles.getDateToString(emp.getFechaNac()));
        lblFIngreso.setText(FuncionesUtiles.getDateToString(emp.getFechaIngreso()));
        lblModDesc.setText(emp.getModalidadDes());
        lblModalidad.setText(emp.getModalidad());
    }

    private void tblResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblResultadosMouseClicked

    private void tblResultadosMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_tblResultadosMouseExited

    private void tblResultadosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tblResultadosMousePressed

    private void tblResultadosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tblResultadosMouseReleased

    private void tblResultadosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblResultadosKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tblResultadosKeyPressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        this.dispose();
        System.exit(0);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        agregarEntSalida(1);
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IngresarAsistencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                IngresarAsistencia dialog = new IngresarAsistencia(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private static javax.swing.JLabel lblApellido;
    private static javax.swing.JLabel lblCedula;
    private static javax.swing.JLabel lblFIngreso;
    private static javax.swing.JLabel lblFNac;
    private javax.swing.JLabel lblHora;
    private static javax.swing.JLabel lblInformacion;
    private static javax.swing.JLabel lblModDesc;
    private static javax.swing.JLabel lblModalidad;
    private static javax.swing.JLabel lblNombre;
    private static javax.swing.JLabel lblTelefono;
    private javax.swing.JPanel panelBase;
    private static javax.swing.JPanel pnlInformacion;
    private javax.swing.JTable tblResultados;
    // End of variables declaration//GEN-END:variables
}
