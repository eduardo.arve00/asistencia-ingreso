/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controldeasistencias.controllers;

import controldeasistencias.controllers.exceptions.NonexistentEntityException;
import controldeasistencias.entidades.HorarioEmp;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Administrador
 */
public class HorarioEmpJpaController implements Serializable {

    public HorarioEmpJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(HorarioEmp horariosEmp) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(horariosEmp);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(HorarioEmp horariosEmp) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            horariosEmp = em.merge(horariosEmp);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = horariosEmp.getId();
                if (findHorarioEmp(id) == null) {
                    throw new NonexistentEntityException("The horariosEmp with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            HorarioEmp horariosEmp;
            try {
                horariosEmp = em.getReference(HorarioEmp.class, id);
                horariosEmp.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The horariosEmp with id " + id + " no longer exists.", enfe);
            }
            em.remove(horariosEmp);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<HorarioEmp> findHorarioEmpEntities() {
        return findHorarioEmpEntities(true, -1, -1);
    }

    public List<HorarioEmp> findHorarioEmpEntities(int maxResults, int firstResult) {
        return findHorarioEmpEntities(false, maxResults, firstResult);
    }

    private List<HorarioEmp> findHorarioEmpEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(HorarioEmp.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public HorarioEmp findHorarioEmp(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(HorarioEmp.class, id);
        } finally {
            em.close();
        }
    }

    public int getHorarioEmpCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<HorarioEmp> rt = cq.from(HorarioEmp.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
