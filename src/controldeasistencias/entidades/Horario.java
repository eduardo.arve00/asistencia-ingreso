/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controldeasistencias.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "horario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Horario.findAll", query = "SELECT h FROM Horario h"),
    @NamedQuery(name = "Horario.findById", query = "SELECT h FROM Horario h WHERE h.id = :id"),
    @NamedQuery(name = "Horario.findByNombre", query = "SELECT h FROM Horario h WHERE h.nombre = :nombre"),
    @NamedQuery(name = "Horario.findByDescripcion", query = "SELECT h FROM Horario h WHERE h.descripcion = :descripcion"),
    @NamedQuery(name = "Horario.findByEntrada", query = "SELECT h FROM Horario h WHERE h.entrada = :entrada"),
    @NamedQuery(name = "Horario.findByHETolerancia", query = "SELECT h FROM Horario h WHERE h.hETolerancia = :hETolerancia"),
    @NamedQuery(name = "Horario.findByHEalmuerzo", query = "SELECT h FROM Horario h WHERE h.hEalmuerzo = :hEalmuerzo"),
    @NamedQuery(name = "Horario.findByHSAlmuerzo", query = "SELECT h FROM Horario h WHERE h.hSAlmuerzo = :hSAlmuerzo"),
    @NamedQuery(name = "Horario.findBySalida", query = "SELECT h FROM Horario h WHERE h.salida = :salida"),
    @NamedQuery(name = "Horario.findByHSTolerancia", query = "SELECT h FROM Horario h WHERE h.hSTolerancia = :hSTolerancia"),
    @NamedQuery(name = "Horario.findByActivo", query = "SELECT h FROM Horario h WHERE h.activo = :activo"),
    @NamedQuery(name = "Horario.findByEliminado", query = "SELECT h FROM Horario h WHERE h.eliminado = :eliminado"),
    @NamedQuery(name = "Horario.findByFecha", query = "SELECT h FROM Horario h WHERE h.fecha = :fecha"),
    @NamedQuery(name = "Horario.findByFechaReg", query = "SELECT h FROM Horario h WHERE h.fechaReg = :fechaReg"),
    @NamedQuery(name = "Horario.findByTipo", query = "SELECT h FROM Horario h WHERE h.tipo = :tipo"),
    @NamedQuery(name = "Horario.findByIdMUsuario", query = "SELECT h FROM Horario h WHERE h.idMUsuario = :idMUsuario")})
public class Horario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "entrada")
    @Temporal(TemporalType.TIME)
    private Date entrada;
    @Column(name = "HETolerancia")
    @Temporal(TemporalType.TIME)
    private Date hETolerancia;
    @Column(name = "HEalmuerzo")
    @Temporal(TemporalType.TIME)
    private Date hEalmuerzo;
    @Column(name = "HSAlmuerzo")
    @Temporal(TemporalType.TIME)
    private Date hSAlmuerzo;
    @Column(name = "salida")
    @Temporal(TemporalType.TIME)
    private Date salida;
    @Column(name = "HSTolerancia")
    @Temporal(TemporalType.TIME)
    private Date hSTolerancia;
    @Column(name = "activo")
    private Boolean activo;
    @Column(name = "eliminado")
    private Boolean eliminado;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "fecha_reg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaReg;
    @Column(name = "tipo")
    private String tipo;
    @Basic(optional = false)
    @Column(name = "id_m_usuario")
    private int idMUsuario;
    @OneToMany(mappedBy = "viernes")
    private Collection<HorarioEmp> horarioEmpCollection;
    @OneToMany(mappedBy = "sabado")
    private Collection<HorarioEmp> horarioEmpCollection1;
    @OneToMany(mappedBy = "miercoles")
    private Collection<HorarioEmp> horarioEmpCollection2;
    @OneToMany(mappedBy = "martes")
    private Collection<HorarioEmp> horarioEmpCollection3;
    @OneToMany(mappedBy = "lunes")
    private Collection<HorarioEmp> horarioEmpCollection4;
    @OneToMany(mappedBy = "jueves")
    private Collection<HorarioEmp> horarioEmpCollection5;
    @OneToMany(mappedBy = "domingo")
    private Collection<HorarioEmp> horarioEmpCollection6;

    public Horario() {
    }

    public Horario(Integer id) {
        this.id = id;
    }

    public Horario(Integer id, int idMUsuario) {
        this.id = id;
        this.idMUsuario = idMUsuario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getEntrada() {
        return entrada;
    }

    public void setEntrada(Date entrada) {
        this.entrada = entrada;
    }

    public Date getHETolerancia() {
        return hETolerancia;
    }

    public void setHETolerancia(Date hETolerancia) {
        this.hETolerancia = hETolerancia;
    }

    public Date getHEalmuerzo() {
        return hEalmuerzo;
    }

    public void setHEalmuerzo(Date hEalmuerzo) {
        this.hEalmuerzo = hEalmuerzo;
    }

    public Date getHSAlmuerzo() {
        return hSAlmuerzo;
    }

    public void setHSAlmuerzo(Date hSAlmuerzo) {
        this.hSAlmuerzo = hSAlmuerzo;
    }

    public Date getSalida() {
        return salida;
    }

    public void setSalida(Date salida) {
        this.salida = salida;
    }

    public Date getHSTolerancia() {
        return hSTolerancia;
    }

    public void setHSTolerancia(Date hSTolerancia) {
        this.hSTolerancia = hSTolerancia;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getEliminado() {
        return eliminado;
    }

    public void setEliminado(Boolean eliminado) {
        this.eliminado = eliminado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getIdMUsuario() {
        return idMUsuario;
    }

    public void setIdMUsuario(int idMUsuario) {
        this.idMUsuario = idMUsuario;
    }

    @XmlTransient
    public Collection<HorarioEmp> getHorarioEmpCollection() {
        return horarioEmpCollection;
    }

    public void setHorarioEmpCollection(Collection<HorarioEmp> horarioEmpCollection) {
        this.horarioEmpCollection = horarioEmpCollection;
    }

    @XmlTransient
    public Collection<HorarioEmp> getHorarioEmpCollection1() {
        return horarioEmpCollection1;
    }

    public void setHorarioEmpCollection1(Collection<HorarioEmp> horarioEmpCollection1) {
        this.horarioEmpCollection1 = horarioEmpCollection1;
    }

    @XmlTransient
    public Collection<HorarioEmp> getHorarioEmpCollection2() {
        return horarioEmpCollection2;
    }

    public void setHorarioEmpCollection2(Collection<HorarioEmp> horarioEmpCollection2) {
        this.horarioEmpCollection2 = horarioEmpCollection2;
    }

    @XmlTransient
    public Collection<HorarioEmp> getHorarioEmpCollection3() {
        return horarioEmpCollection3;
    }

    public void setHorarioEmpCollection3(Collection<HorarioEmp> horarioEmpCollection3) {
        this.horarioEmpCollection3 = horarioEmpCollection3;
    }

    @XmlTransient
    public Collection<HorarioEmp> getHorarioEmpCollection4() {
        return horarioEmpCollection4;
    }

    public void setHorarioEmpCollection4(Collection<HorarioEmp> horarioEmpCollection4) {
        this.horarioEmpCollection4 = horarioEmpCollection4;
    }

    @XmlTransient
    public Collection<HorarioEmp> getHorarioEmpCollection5() {
        return horarioEmpCollection5;
    }

    public void setHorarioEmpCollection5(Collection<HorarioEmp> horarioEmpCollection5) {
        this.horarioEmpCollection5 = horarioEmpCollection5;
    }

    @XmlTransient
    public Collection<HorarioEmp> getHorarioEmpCollection6() {
        return horarioEmpCollection6;
    }

    public void setHorarioEmpCollection6(Collection<HorarioEmp> horarioEmpCollection6) {
        this.horarioEmpCollection6 = horarioEmpCollection6;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Horario)) {
            return false;
        }
        Horario other = (Horario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "controldeasistencias.entidades.Horario[ id=" + id + " ]";
    }
    
}
