/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;

/**
 *
 * @author Administrador
 */
public class ValidarNumero implements java.awt.event.KeyListener {

    JTextField jtext;

    public ValidarNumero(JTextField jtext) {
        this.jtext = jtext;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        String c = new String();
        try {
            if (!c.isEmpty()) {
                c = jtext.getText(jtext.getText().length() - 1, jtext.getText().length());
            }
        } catch (BadLocationException ex) {
            Logger.getLogger(ValidarNumero.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (!jtext.getText().isEmpty()) {

            try {
                Integer.parseInt(jtext.getText().trim());
            } catch (NumberFormatException nfe) {
                System.out.println("key: " + e.getKeyChar());
                e.consume();
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
