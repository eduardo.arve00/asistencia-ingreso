/**
 * ************************************************************************
 *
 * JHTML - Cross Platform HTML Design Environment Copyright (C) 2000 Riyad Kalla
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Created: 6/14/2000 File Name: SplashScreen.java File Desc: Displays
 * splashscreen. Responsibilities: Displays a splash screen for JHTML startup as
 * well as progress bar and text.
 *
 *************************************************************************
 */
package controldeasistencias.utils.splashscreen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;
import controldeasistencias.ControlDeAsistencias;
import utils.PreferenceUtility;

public class SplashScreen extends JWindow {

    private int windowWidth = 0;
    private int windowHeight = 0;
    private JLabel progressLabel = null;
    private SplashScreenBackground splashScreenBackground = null;

    public SplashScreen(int numberOfSteps) {
        splashScreenBackground = new SplashScreenBackground();

        progressLabel = new JLabel(" ");
        progressLabel.setForeground(Color.white);
        progressLabel.setBorder(BorderFactory.createLineBorder(Color.darkGray));

        getContentPane().setLayout(new BorderLayout());
        getContentPane().setBackground(new Color(0, 0, 100));
        getContentPane().add(splashScreenBackground, BorderLayout.CENTER);
        getContentPane().add(progressLabel, BorderLayout.SOUTH);
        getContentPane().validate();
        setSize(windowWidth, windowHeight + (int) progressLabel.getPreferredSize().getHeight());
        setLocation(ScreenUtility.getCenterCoordinates(windowWidth, windowHeight));
        setVisible(true);
        toFront();
    }

    public void update(String progressName) {
        progressLabel.setText(" " + progressName);
    }

    private class SplashScreenBackground extends JPanel {

        private Image backgroundImage = null;
        private MediaTracker imageLoadTracker = null;

        public SplashScreenBackground() {
            setBorder(BorderFactory.createLineBorder(Color.darkGray));

            backgroundImage = Toolkit.getDefaultToolkit().getImage(PreferenceUtility.BaseDir + "resources/graphics/images/SplashScreenNewLogo.png");
            imageLoadTracker = new MediaTracker(this);
            imageLoadTracker.addImage(backgroundImage, 0);

            try {
                imageLoadTracker.waitForID(0);
            } catch (Exception e) {
                System.err.println(e);
            }

            windowWidth = backgroundImage.getWidth(this);
            windowHeight = backgroundImage.getHeight(this);
        }

        public void paintComponent(Graphics g) {
            g.drawImage(backgroundImage, 0, 0, this);
            //g.drawString( net.sourceforge.jhtml.JHTML.getMajorVersion(), windowWidth - ( windowWidth - 20 ),  windowHeight - 10 );
            g.setColor(Color.WHITE);
            g.drawString(" v " +ControlDeAsistencias.getMajorVersion() + "." + ControlDeAsistencias.getMinorVersion() + "." + ControlDeAsistencias.getPatchVersion(), (windowWidth - 55), windowHeight - 10);

	    //g.drawString( net.sourceforge.jhtml.JHTML.getMinorVersion(), windowWidth - ( windowWidth - 40 ),  windowHeight - 10 );
        }
    }
}
