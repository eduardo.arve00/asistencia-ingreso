/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controldeasistencias.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Administrador
 */
public class FuncionesUtiles {
    public static String getDateToString (Date date) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
        return formatoDeFecha.format(date); 
    }
    
    public static String getDateToStringWhitSecongs(Date date) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return formatoDeFecha.format(date); 
    }
    
    public static String getCurrentTime(Date date) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("HH:mm:ss");
        return formatoDeFecha.format(date); 
    }
    
    public static String getTimeHHmm(Date date) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("HH:mm");
        return formatoDeFecha.format(date); 
    }
}
