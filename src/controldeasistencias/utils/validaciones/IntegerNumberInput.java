/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.utils.validaciones;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JTextField;

/**
 *
 * @author Administrador
 */
public class IntegerNumberInput implements KeyListener {

    JTextField field;

    public IntegerNumberInput(JTextField field) {
        this.field = field;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        char c = ke.getKeyChar();

        if (!Character.isDigit(c)) {

            ke.consume();

            field.setToolTipText("Ingresa Solo Numeros");
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
