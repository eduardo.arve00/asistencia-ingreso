/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.utils.validaciones;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JTextField;

/**
 *
 * @author Administrador
 */
public class FormatCedulaInput implements KeyListener {

    JTextField field;
    String cedulas = "VvEe";

    public FormatCedulaInput(JTextField field) {
        this.field = field;
    }

    //CI:  V-19709183
    //RIF: V-19709183-1
    @Override
    public void keyTyped(KeyEvent e) {

        //el primer caracter debe ser tipo caracter 
        //solo se permite la V, E, J o G 
        /**
         * si el texto esta en blanco se esta intentando agregar el primer
         * caracter. este caracter debe ser una de las letras permitidas para el
         * tipo de letra. si no se encuentra el caracter especificado para la
         * primera letra se ignora el evento.
         */
        if (field.getText().length() == 0) {
            if (!cedulas.contains(String.valueOf(e.getKeyChar()))) {
                e.consume();
            }
        } else if (field.getText().length() == 1) {
            /**
             * si se ha pulsado la tecla de borrado lo que hace que se elimine
             * el caracter '-' del texto dando como resultado que la longitud de
             * la cadena sea igual a 1, entonces se borra automaticamente el
             * tipo de cedula insertada
             */
            field.setText("");
        } else if (field.getText().length() < 10) {
            //si el caracter insertado no es un numero 
            if (!Character.isDigit(e.getKeyChar())) {
                e.consume(); //ignorar la orden.
            }
        } else {
            //ignorar si no esta dentro de los 10 caracteres
            e.consume();
        }

    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (field.getText().length() == 1) {
            //convierte automaticamente los caracteres en mayuscula.
            //y le agrega un guion para separar
            field.setText(field.getText().toUpperCase() + "-");
        }
    }

}
