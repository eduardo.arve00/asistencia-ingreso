/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controldeasistencias.utils.validaciones;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JTextField;

/**
 *
 * @author Administrador
 */
public class TelephoneFormatInput implements KeyListener {

    JTextField field;

    public TelephoneFormatInput(JTextField field) {
        this.field = field;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        //0412-4925665
        if (field.getText().length() == 4) {
            field.setText(field.getText().substring(0, 3));
        } 
        
        if (field.getText().length() > 11) {
            ke.consume();
            return;
        }

        char c = ke.getKeyChar();

        if (!Character.isDigit(c)) {

            ke.consume();

            field.setToolTipText("Ingresa Solo Numeros");
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
       
        
        if (field.getText().length() == 4) {
            field.setText(field.getText() + "-");
        }
    }

    public boolean isCorrect() {
        return true;
    }
}

