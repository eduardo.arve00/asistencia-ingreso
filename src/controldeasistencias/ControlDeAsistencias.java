/**
 * ************************************************************************
 *
 * BioAsis - Control de Asistencia Biometrico Copyright(C) 2014 Eduardo Arvelaiz
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Created: 9/21/2014 File Name: ControlDeAsistencias.java File Desc: Main class
 * Responsibilities: Init LogUtility, init TextUtility, invoke dependency che
 * cking, invoke preference loading, init the build of the program interface,
 * handle exiting JHTML back to system.
 *
 *************************************************************************
 */
package controldeasistencias;

import com.griaule.fingerprintsdk.UtilSDK;
import com.mysql.jdbc.CommunicationsException;
import controldeasistencias.controllers.UsuarioJpaController;
import controldeasistencias.ingresar.IngresarAsistencia;
import controldeasistencias.utils.splashscreen.SplashScreen;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.PersistenceException;
import javax.swing.JOptionPane;
import persistence.util.JpaUtil;
import controldeasistencias.utils.BDConfig;
import controldeasistencias.utils.Boot;
import javax.swing.JFrame;
import utils.LogUtility;
import utils.PreferenceUtility;

public class ControlDeAsistencias {

    private static SplashScreen splashScreen = null;

    private static final String MAJOR_VERSION = "1";
    private static final String MINOR_VERSION = "0";
    private static final String PATCH_VERSION = "0";

    private static boolean loadPreferences(String baseDir) {
        splashScreen.update("Iniciando: Preferencias...");
        return PreferenceUtility.initPreferenceUtility(baseDir);
    }

    private static boolean initConnectToBD() throws CommunicationsException {
        splashScreen.update("Iniciando: Conexión con la base de datos....");
        UsuarioJpaController controller = new UsuarioJpaController(JpaUtil.getEntityManagerFactory(Boot.getProperties()));
        controller.getUsuarioCount();
        return true;
    }

    private static boolean initLogUtility() {
        splashScreen.update("Iniciando: Log...");

        boolean initStatus = LogUtility.initLogUtility();

        if (Boolean.valueOf(PreferenceUtility.getLogPreferences().getProperty("clearLogsOnStartup")).booleanValue() == true) {
            LogUtility.clearLogs();
        }

        return initStatus;
    }

    private static boolean initNetworkUtility() {
        splashScreen.update("Iniciando: Conexion de red...");
        return true;
    }

    public static void main(String[] args) {
        //iniciarSDK();
        Boot.getProperties();

        String baseDir = "";
        if (args.length > 0) {
            baseDir = args[0];
        }
        splashScreen = new SplashScreen(10);

        try {
            if (!(initLogUtility() && setUI() && initNetworkUtility() && initConnectToBD())) {
                exitToSystem();
            }
        } catch (CommunicationsException | PersistenceException pe) {
            showErrorDeConexion(pe);
        }

        try {
            IngresarAsistencia asistencia = new IngresarAsistencia(null, true);
            asistencia.setLocationRelativeTo(null);
            asistencia.setExtendedState(JFrame.MAXIMIZED_BOTH);
            splashScreen.update("Iniciando programa...");
            Thread.sleep(1500);
            asistencia.setVisible(true);
            asistencia.toFront();
            
        } catch (InterruptedException ex) {
            Logger.getLogger(ControlDeAsistencias.class.getName()).log(Level.SEVERE, null, ex);
        }
        splashScreen.dispose();

        
    }

    /**
     * Contiene todas las configuraciones inciales del SDK para el Captahuellas
     */
    private static void iniciarSDK() {
        //inicializa la ruta del SDK
        String grFingerNativeDirectory = new File(".").getAbsolutePath();
        UtilSDK.setFingerprintSDKNativeDirectory(grFingerNativeDirectory);

        //
    }

    public static void showErrorDeConexion(Exception ex) {
        int res = JOptionPane.showConfirmDialog(null, "No se ha podido Conectar con la base de datos", "Error", JOptionPane.YES_NO_OPTION);
        LogUtility.writeToErrorLog(ex.getMessage());
        Logger.getLogger(ControlDeAsistencias.class.getName()).log(Level.SEVERE, null, ex);

        if (res == JOptionPane.YES_OPTION) {
            //mostrar cuadro de configuracion de la base de datos
            BDConfig config = new BDConfig(null, true);
            config.setLocationRelativeTo(null);
            config.setVisible(true);
        } else {
            //salir del sistema
            System.exit(1);
        }
    }

    public static String getMajorVersion() {
        return MAJOR_VERSION;
    }

    public static String getMinorVersion() {
        return MINOR_VERSION;
    }

    public static String getPatchVersion() {
        return PATCH_VERSION;
    }

    public static void exitToSystem() {
        System.out.println("Guardando: configuración...");
        PreferenceUtility.storeAllPreferences();

        System.out.println("Cerrando aplicacion...");
        System.exit(0);
    }

    private static boolean setUI() {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IngresarAsistencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return true;

    }
}
