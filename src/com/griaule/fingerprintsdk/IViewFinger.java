/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.griaule.fingerprintsdk;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 *
 * @author Administrador
 */
public interface IViewFinger {

    void setColorLog(Color bgLog);

    void writeLog(String text);

    void showImage(BufferedImage image);

    public void enableImage();

    public void enableTemplate();
}
